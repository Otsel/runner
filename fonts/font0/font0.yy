{
    "id": "b6932029-ce2b-4a0a-9426-09bb9bf38e80",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "font0",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 1,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "KenVector Bold",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "244cdf50-84d6-4374-95d0-bd8377bac056",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 33,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "25c470a0-dc88-46a7-8a6d-e9eb0a778553",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 33,
                "offset": 0,
                "shift": 11,
                "w": 6,
                "x": 54,
                "y": 142
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "9ca21a44-9103-47f3-9131-972e3382495e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 33,
                "offset": 0,
                "shift": 16,
                "w": 8,
                "x": 44,
                "y": 142
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "be4a274d-ff79-4866-9e14-8e41b223d43c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 33,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 29,
                "y": 142
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "fb2d9ac0-a9bf-479a-8213-35877344595b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 33,
                "offset": 1,
                "shift": 13,
                "w": 10,
                "x": 17,
                "y": 142
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "0c2c3008-cd04-4c5a-baab-4986a6a84eb0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 33,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 2,
                "y": 142
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "7330fc78-ef06-4147-9890-77ce2feb7931",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 33,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 237,
                "y": 107
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "9e9c2452-267e-48b1-9408-649c2f232678",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 33,
                "offset": 0,
                "shift": 11,
                "w": 6,
                "x": 229,
                "y": 107
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "b77d328d-e893-485b-aa59-50af5634fc3d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 33,
                "offset": 3,
                "shift": 13,
                "w": 7,
                "x": 220,
                "y": 107
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "799a8593-a0d0-410a-91e4-27445e423f5b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 33,
                "offset": 3,
                "shift": 13,
                "w": 7,
                "x": 211,
                "y": 107
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "7ba403d0-b081-459f-8ff0-b233ff6aa891",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 33,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 62,
                "y": 142
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "2babdad1-c84a-412f-a41a-55447195085a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 33,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 198,
                "y": 107
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "bc6bbee0-c876-43c9-86b4-53cdf65416b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 33,
                "offset": 0,
                "shift": 11,
                "w": 6,
                "x": 172,
                "y": 107
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "a0352796-dc2e-4dc8-b41d-9945e0dbcfa1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 33,
                "offset": 2,
                "shift": 13,
                "w": 8,
                "x": 162,
                "y": 107
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "00fd73af-43fb-499c-9528-4afaa770fd89",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 33,
                "offset": 0,
                "shift": 11,
                "w": 6,
                "x": 154,
                "y": 107
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "787f9a4d-d3ff-4feb-b8db-29401abfdbdb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 33,
                "offset": 2,
                "shift": 13,
                "w": 9,
                "x": 143,
                "y": 107
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "98cad90b-e3b7-497b-94ea-e33c981a3bf2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 33,
                "offset": 0,
                "shift": 21,
                "w": 16,
                "x": 125,
                "y": 107
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "afba6808-1d69-45fa-a7bb-4cffef0d6dd5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 33,
                "offset": 0,
                "shift": 17,
                "w": 12,
                "x": 111,
                "y": 107
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "13c08280-c2a6-42a0-a796-795818ffe102",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 33,
                "offset": 0,
                "shift": 21,
                "w": 16,
                "x": 93,
                "y": 107
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "e21f3e4f-ab9b-4d33-8935-fdfc4dffbacc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 33,
                "offset": 0,
                "shift": 22,
                "w": 17,
                "x": 74,
                "y": 107
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "6c2ab2aa-2ff8-4a36-938f-db38eb4fba30",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 33,
                "offset": 0,
                "shift": 21,
                "w": 16,
                "x": 56,
                "y": 107
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "1d391791-1f35-4d3c-abea-59139100bb41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 33,
                "offset": 0,
                "shift": 21,
                "w": 16,
                "x": 180,
                "y": 107
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "ce907aee-bc25-4699-b5d6-3160dfb20122",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 33,
                "offset": 0,
                "shift": 21,
                "w": 16,
                "x": 75,
                "y": 142
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "77ad7374-558b-4481-a476-3eb9009af390",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 33,
                "offset": 0,
                "shift": 22,
                "w": 17,
                "x": 93,
                "y": 142
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "d3e30ce4-bcfe-468a-b519-7f1d5fdc4846",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 33,
                "offset": 0,
                "shift": 21,
                "w": 16,
                "x": 112,
                "y": 142
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "cb86df46-6b21-4bbf-9664-a084c9d581ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 33,
                "offset": 0,
                "shift": 21,
                "w": 16,
                "x": 194,
                "y": 177
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "7b39ee88-7654-473a-ad52-c04f89be3533",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 33,
                "offset": 5,
                "shift": 13,
                "w": 3,
                "x": 189,
                "y": 177
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "32e34259-d211-4bec-9caf-42cd5610f11b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 33,
                "offset": 4,
                "shift": 13,
                "w": 4,
                "x": 183,
                "y": 177
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "a8fb1d96-4f60-405a-a0bf-635dc9013ecc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 33,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 170,
                "y": 177
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "3ccefebe-abe2-4354-bf0d-92a284bc8514",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 33,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 157,
                "y": 177
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "5394d9dd-c7d7-48d5-97f8-5052cbe114ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 33,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 144,
                "y": 177
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "3dd34488-f307-429c-8191-749dd6146ed2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 33,
                "offset": 0,
                "shift": 22,
                "w": 17,
                "x": 125,
                "y": 177
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "c7cfcf41-6340-4197-b034-409737afa06b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 33,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 110,
                "y": 177
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "c022f1c6-764c-4da1-966c-8676fb4e7937",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 33,
                "offset": 0,
                "shift": 21,
                "w": 16,
                "x": 92,
                "y": 177
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "8d725370-ce52-41aa-ae70-f434005169cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 33,
                "offset": 0,
                "shift": 21,
                "w": 16,
                "x": 74,
                "y": 177
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "83fd5ecd-bf54-41f8-8d71-50aed69d7d41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 33,
                "offset": 0,
                "shift": 21,
                "w": 16,
                "x": 56,
                "y": 177
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "f25d21ee-5e2a-42fe-b815-7642b9bae0f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 33,
                "offset": 0,
                "shift": 21,
                "w": 16,
                "x": 38,
                "y": 177
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "b02db975-dc4f-4bb7-aede-1283d3848ff7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 33,
                "offset": 0,
                "shift": 21,
                "w": 16,
                "x": 20,
                "y": 177
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "e939b53c-165d-4bc9-9469-ad79b209c5e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 33,
                "offset": 0,
                "shift": 21,
                "w": 16,
                "x": 2,
                "y": 177
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "8e219b2d-c59c-47b6-a83f-572d1a2784e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 33,
                "offset": 0,
                "shift": 21,
                "w": 16,
                "x": 229,
                "y": 142
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "78b6e154-0db4-4eb7-b675-77444e0547e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 33,
                "offset": 0,
                "shift": 21,
                "w": 16,
                "x": 211,
                "y": 142
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "d3d0a41b-6d20-48b4-bd81-49a024700603",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 33,
                "offset": 0,
                "shift": 11,
                "w": 6,
                "x": 203,
                "y": 142
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "326f17f4-a084-4de0-9650-42b4cc107076",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 33,
                "offset": 0,
                "shift": 22,
                "w": 17,
                "x": 184,
                "y": 142
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "60255b7a-cdde-410c-b41f-b2e111bade8e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 33,
                "offset": 0,
                "shift": 21,
                "w": 16,
                "x": 166,
                "y": 142
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "25e26051-5559-4290-8932-eb15294f9852",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 33,
                "offset": 0,
                "shift": 21,
                "w": 16,
                "x": 148,
                "y": 142
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "16c37818-2fe3-4d2c-9ba4-f0212f120519",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 33,
                "offset": 0,
                "shift": 21,
                "w": 16,
                "x": 130,
                "y": 142
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "246a9793-78f4-4351-b341-d699329161aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 33,
                "offset": 0,
                "shift": 21,
                "w": 16,
                "x": 38,
                "y": 107
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "f4686f34-6b01-412c-adf4-d9bfefb80f4e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 33,
                "offset": 0,
                "shift": 21,
                "w": 16,
                "x": 20,
                "y": 107
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "5a4bb11e-694b-4d4f-9515-bbcf69653220",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 33,
                "offset": 0,
                "shift": 21,
                "w": 16,
                "x": 2,
                "y": 107
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "2edb5854-bbe0-452f-8a5e-aedd41ab970f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 33,
                "offset": 0,
                "shift": 21,
                "w": 16,
                "x": 100,
                "y": 37
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "ff9b3e17-185d-4695-9b99-f14d523b4de8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 33,
                "offset": 0,
                "shift": 21,
                "w": 16,
                "x": 74,
                "y": 37
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "37c17ba4-86b9-491f-8208-80d4e0054f02",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 33,
                "offset": 0,
                "shift": 21,
                "w": 16,
                "x": 56,
                "y": 37
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "e3448d26-57db-4ac1-86c4-740a1d9d9aee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 33,
                "offset": 0,
                "shift": 21,
                "w": 16,
                "x": 38,
                "y": 37
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "5e2829d2-35bb-42fb-b3fd-dd0cd8549e05",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 33,
                "offset": 0,
                "shift": 21,
                "w": 16,
                "x": 20,
                "y": 37
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "a58878e9-83b8-4038-a3a5-b322de06adc6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 33,
                "offset": 0,
                "shift": 21,
                "w": 16,
                "x": 2,
                "y": 37
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "a9608f02-476d-41cb-a0c8-a9e00be86db2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 33,
                "offset": 0,
                "shift": 21,
                "w": 16,
                "x": 230,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "a874f555-d2ba-4bd0-a221-b781e734d887",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 33,
                "offset": 0,
                "shift": 21,
                "w": 16,
                "x": 212,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "d2e6c72e-eac8-478b-80ee-7b03a67f9c38",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 33,
                "offset": 0,
                "shift": 22,
                "w": 17,
                "x": 193,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "47e95a2d-3ad5-446a-8c11-82c9a03f6d85",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 33,
                "offset": 0,
                "shift": 21,
                "w": 16,
                "x": 175,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "6b7f5cf9-c39f-47bd-9de7-ffbb1b008505",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 33,
                "offset": 4,
                "shift": 13,
                "w": 6,
                "x": 92,
                "y": 37
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "7f9b4514-3d74-47bd-bc1e-378e9f6b5b43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 33,
                "offset": 2,
                "shift": 13,
                "w": 9,
                "x": 164,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "3567351a-0b0b-4237-8c14-90078f91e3b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 33,
                "offset": 2,
                "shift": 13,
                "w": 7,
                "x": 137,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "2459d35b-7667-470f-a2cb-ced6172dbb74",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 33,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 124,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "ccb82272-8fa5-40d1-8898-0671454ccd0f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 33,
                "offset": 0,
                "shift": 21,
                "w": 16,
                "x": 106,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "2eb8f733-101d-4521-be2c-478624aa9f93",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 33,
                "offset": 4,
                "shift": 13,
                "w": 5,
                "x": 99,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "c7a69527-4919-4852-adcc-b0fea1c7acc1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 33,
                "offset": 0,
                "shift": 21,
                "w": 16,
                "x": 81,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "d56eec9e-d17c-4dcc-8685-4f36013096cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 33,
                "offset": 0,
                "shift": 21,
                "w": 16,
                "x": 63,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "225832e6-3b86-46c7-823b-bae5cde071f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 33,
                "offset": 0,
                "shift": 21,
                "w": 16,
                "x": 45,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "7b2e1c7d-b3c2-4cfd-90bf-0638ffac2b4c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 33,
                "offset": 0,
                "shift": 21,
                "w": 16,
                "x": 27,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "c518f9b6-0343-4a82-83d0-e785d7757c3b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 33,
                "offset": 0,
                "shift": 21,
                "w": 16,
                "x": 9,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "5c24134f-e927-4a81-989b-c1059d44bcd3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 33,
                "offset": 0,
                "shift": 21,
                "w": 16,
                "x": 146,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "4909f32f-39de-472c-a95e-2e69d86825f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 33,
                "offset": 0,
                "shift": 21,
                "w": 16,
                "x": 118,
                "y": 37
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "fe8c400e-3193-485b-abb6-8bd00a360c46",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 33,
                "offset": 0,
                "shift": 21,
                "w": 16,
                "x": 38,
                "y": 72
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "f6dcea8d-10ae-402d-9a22-be04c2b36390",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 33,
                "offset": 0,
                "shift": 11,
                "w": 6,
                "x": 136,
                "y": 37
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "6d278d0d-d992-47af-9b4d-42a97a0180e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 33,
                "offset": 0,
                "shift": 22,
                "w": 17,
                "x": 218,
                "y": 72
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "1f1f58f1-1847-4db0-b9dd-109bbdce9652",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 33,
                "offset": 0,
                "shift": 21,
                "w": 16,
                "x": 200,
                "y": 72
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "c241b758-fd86-4b93-b504-d7e827fa025c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 33,
                "offset": 0,
                "shift": 21,
                "w": 16,
                "x": 182,
                "y": 72
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "7677d8be-d26d-4c37-93f7-69ffb9b8cc40",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 33,
                "offset": 0,
                "shift": 21,
                "w": 16,
                "x": 164,
                "y": 72
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "e954edf4-2c82-4249-a24a-8c0de6118517",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 33,
                "offset": 0,
                "shift": 21,
                "w": 16,
                "x": 146,
                "y": 72
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "1bb019ee-c0ff-4299-a4fd-519dd1e8f61f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 33,
                "offset": 0,
                "shift": 21,
                "w": 16,
                "x": 128,
                "y": 72
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "51322cff-2977-4c33-ba75-fe05d426593a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 33,
                "offset": 0,
                "shift": 21,
                "w": 16,
                "x": 110,
                "y": 72
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "d2752708-9a16-442e-928a-306873308ea9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 33,
                "offset": 0,
                "shift": 21,
                "w": 16,
                "x": 92,
                "y": 72
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "05f40e4f-df4d-432b-8e9e-0ec73e74c462",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 33,
                "offset": 0,
                "shift": 21,
                "w": 16,
                "x": 74,
                "y": 72
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "426e53c1-1fda-4740-b7da-2ee1467b813e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 33,
                "offset": 0,
                "shift": 21,
                "w": 16,
                "x": 237,
                "y": 72
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "00f0698e-b842-4272-96dd-55a070c7434f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 33,
                "offset": 0,
                "shift": 21,
                "w": 16,
                "x": 56,
                "y": 72
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "389df638-6018-4483-80c2-73fab3fcdd1d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 33,
                "offset": 0,
                "shift": 21,
                "w": 16,
                "x": 20,
                "y": 72
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "f4c91e71-b44a-4beb-bb4b-1cf33a043f10",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 33,
                "offset": 0,
                "shift": 21,
                "w": 16,
                "x": 2,
                "y": 72
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "f4bf833e-a161-493d-acc9-bee2a219112b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 33,
                "offset": 0,
                "shift": 21,
                "w": 16,
                "x": 226,
                "y": 37
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "3cb5870d-2314-43b4-a7fb-5c977b2a7bb9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 33,
                "offset": 0,
                "shift": 21,
                "w": 16,
                "x": 208,
                "y": 37
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "1c4d4d9a-8b55-475e-a92b-1eb8fdd6a6ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 33,
                "offset": 0,
                "shift": 22,
                "w": 17,
                "x": 189,
                "y": 37
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "92ab25a9-3f7c-4056-b25c-51c249b54db7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 33,
                "offset": 0,
                "shift": 21,
                "w": 16,
                "x": 171,
                "y": 37
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "51fc7249-d18d-4830-a1d5-39bac22d99fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 33,
                "offset": 2,
                "shift": 13,
                "w": 9,
                "x": 160,
                "y": 37
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "fe84b569-2719-4080-9468-ee0b1ab17f1b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 33,
                "offset": 5,
                "shift": 13,
                "w": 3,
                "x": 155,
                "y": 37
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "c851f2a0-9b9e-4e4b-adad-3dd501a7bba1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 33,
                "offset": 2,
                "shift": 13,
                "w": 9,
                "x": 144,
                "y": 37
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "dd4a4805-6709-4440-81d7-b965b3c6592e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 33,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 212,
                "y": 177
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "252634cd-27cb-4d94-b830-f1acea224b6f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 33,
                "offset": 4,
                "shift": 20,
                "w": 13,
                "x": 225,
                "y": 177
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 16,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}