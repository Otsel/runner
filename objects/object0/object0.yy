{
    "id": "21b06c7f-a8e8-4a17-a3de-5f6d2eb8df3b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object0",
    "eventList": [
        {
            "id": "63466838-6716-4d50-bf31-8a275a290a8b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "21b06c7f-a8e8-4a17-a3de-5f6d2eb8df3b"
        },
        {
            "id": "53adab71-0a5c-4ca8-846d-0b59ca6bf18f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "21b06c7f-a8e8-4a17-a3de-5f6d2eb8df3b"
        },
        {
            "id": "37fe88bf-142f-4d1f-aaf5-2b8065c376e6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 7,
            "m_owner": "21b06c7f-a8e8-4a17-a3de-5f6d2eb8df3b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "234dab79-8adc-44a4-827d-957a3e7f2bd8",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "c78cf8f3-da87-49e0-8b4e-7ab571ac953a",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 0
        },
        {
            "id": "7dbca7a1-e612-4955-906e-c96d0121b4b2",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 64
        },
        {
            "id": "69681a25-edfc-4a2e-bd53-bca48dc9ab8d",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 64
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "3bbda542-9de7-4a36-a4e4-8f127b63a83e",
    "visible": true
}