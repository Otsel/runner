{
    "id": "cc2f4c53-1b47-4734-839e-9c85a524ac53",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "spikey",
    "eventList": [
        {
            "id": "ea31a693-ccc3-4597-b8a6-328f819e8597",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "cc2f4c53-1b47-4734-839e-9c85a524ac53"
        },
        {
            "id": "20afbc82-dc6b-4551-bb1d-216e26df33fd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "cc2f4c53-1b47-4734-839e-9c85a524ac53"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "11713623-2cb7-461d-91cc-c47f3fb6e624",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "42b67519-8c49-4284-8660-422e4a32074e",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 0
        },
        {
            "id": "38fa8bed-2923-43ab-bfe9-d6d8c1ca49b8",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 64
        },
        {
            "id": "7bc1f03e-2654-43dc-bcc6-f0b5da49b850",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 64
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "16049877-860d-426f-ba19-9378128fdc2d",
    "visible": true
}