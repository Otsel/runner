{
    "id": "9ba456eb-5720-4eef-8546-751aa394b8cc",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object1",
    "eventList": [
        {
            "id": "be373d7b-ae41-4db4-a6de-962db538f9a0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "21b06c7f-a8e8-4a17-a3de-5f6d2eb8df3b",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "9ba456eb-5720-4eef-8546-751aa394b8cc"
        },
        {
            "id": "705dc84f-c392-468b-a630-38056c8cc94e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 38,
            "eventtype": 9,
            "m_owner": "9ba456eb-5720-4eef-8546-751aa394b8cc"
        },
        {
            "id": "e7346864-b2d7-4a07-bf21-ec36ba152426",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "9ba456eb-5720-4eef-8546-751aa394b8cc"
        },
        {
            "id": "fd44a570-53bb-401b-bc41-15db8981127c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9ba456eb-5720-4eef-8546-751aa394b8cc"
        },
        {
            "id": "f54b26ad-0adc-4774-ba8e-643193bb6e96",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "9ba456eb-5720-4eef-8546-751aa394b8cc"
        },
        {
            "id": "8b2f6ab2-ddfb-42a3-bbf8-55e770881384",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 3,
            "eventtype": 2,
            "m_owner": "9ba456eb-5720-4eef-8546-751aa394b8cc"
        },
        {
            "id": "f6d71f93-a8f0-4661-9c21-1fa7cb570ec6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "9ba456eb-5720-4eef-8546-751aa394b8cc"
        },
        {
            "id": "981d6305-1cfd-4607-93c2-69c85de5d3f3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "cc2f4c53-1b47-4734-839e-9c85a524ac53",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "9ba456eb-5720-4eef-8546-751aa394b8cc"
        },
        {
            "id": "0577c324-c65d-4e5a-8ed0-49deed6c8eb8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 2,
            "m_owner": "9ba456eb-5720-4eef-8546-751aa394b8cc"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "f507832d-2f62-4bc7-b240-0392c02a7506",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "f7bf128c-97f6-460d-b41c-39777c9636ea",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 0
        },
        {
            "id": "824b4f83-1299-40b6-9bd3-2329fca824f1",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 64
        },
        {
            "id": "5de20ed7-1251-45b0-a3bb-f6e97f89c13f",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 64
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "61894e4d-f8a2-48fd-bdcc-7dfd712a2637",
    "visible": true
}