{
    "id": "d1b317a5-c448-4d35-99c5-2708c3500ca8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bad2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 61,
    "bbox_left": 4,
    "bbox_right": 66,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "baa7d3d4-bfbb-43ae-8d9a-28869d45a85f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d1b317a5-c448-4d35-99c5-2708c3500ca8",
            "compositeImage": {
                "id": "1770fe33-1939-4fc5-ac73-95ee614004fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "baa7d3d4-bfbb-43ae-8d9a-28869d45a85f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d4769b3-e21d-4f89-ad7b-b39d96dbacd5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "baa7d3d4-bfbb-43ae-8d9a-28869d45a85f",
                    "LayerId": "1bdfc6dc-80d2-4ac3-9002-282a0af4c61b"
                }
            ]
        },
        {
            "id": "93afcd8c-c7c8-4174-9e4a-1091e88b3e7a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d1b317a5-c448-4d35-99c5-2708c3500ca8",
            "compositeImage": {
                "id": "2a6f77c3-c678-4391-ba71-7d465ea74756",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "93afcd8c-c7c8-4174-9e4a-1091e88b3e7a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae772dca-72d2-479b-ae1d-eee3e8deecb7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93afcd8c-c7c8-4174-9e4a-1091e88b3e7a",
                    "LayerId": "1bdfc6dc-80d2-4ac3-9002-282a0af4c61b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 72,
    "layers": [
        {
            "id": "1bdfc6dc-80d2-4ac3-9002-282a0af4c61b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d1b317a5-c448-4d35-99c5-2708c3500ca8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 6,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 72,
    "xorig": 0,
    "yorig": 71
}