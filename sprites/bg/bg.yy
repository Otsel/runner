{
    "id": "368b403e-483e-4705-970d-db5aed7c5e62",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1023,
    "bbox_left": 0,
    "bbox_right": 1023,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ef6fc561-58ed-49c7-893c-9a7fd13aa599",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "368b403e-483e-4705-970d-db5aed7c5e62",
            "compositeImage": {
                "id": "19280eb8-b709-4c58-b550-a058b535b723",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef6fc561-58ed-49c7-893c-9a7fd13aa599",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1cc497bf-f399-4fd5-ba2e-5c970d817692",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef6fc561-58ed-49c7-893c-9a7fd13aa599",
                    "LayerId": "c3a8a511-7466-4020-be1f-9c7f542b4dd4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1024,
    "layers": [
        {
            "id": "c3a8a511-7466-4020-be1f-9c7f542b4dd4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "368b403e-483e-4705-970d-db5aed7c5e62",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1024,
    "xorig": 0,
    "yorig": 0
}