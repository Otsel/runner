{
    "id": "16049877-860d-426f-ba19-9378128fdc2d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bad",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 69,
    "bbox_left": 10,
    "bbox_right": 59,
    "bbox_top": 36,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "09430804-0393-4bb9-b7e2-63bb2e450b84",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16049877-860d-426f-ba19-9378128fdc2d",
            "compositeImage": {
                "id": "18832ca9-5d96-413b-b6be-cb3031fd10c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "09430804-0393-4bb9-b7e2-63bb2e450b84",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8bcfcd76-dc6f-4e1e-8fb3-18a268bc3aca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "09430804-0393-4bb9-b7e2-63bb2e450b84",
                    "LayerId": "bb3c81d2-18a4-474f-8b2f-3e077340284f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 72,
    "layers": [
        {
            "id": "bb3c81d2-18a4-474f-8b2f-3e077340284f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "16049877-860d-426f-ba19-9378128fdc2d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 6,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 72,
    "xorig": 0,
    "yorig": 71
}