{
    "id": "61894e4d-f8a2-48fd-bdcc-7dfd712a2637",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 93,
    "bbox_left": 4,
    "bbox_right": 67,
    "bbox_top": 2,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "49633122-63ee-49ae-b730-7edb78d7c817",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61894e4d-f8a2-48fd-bdcc-7dfd712a2637",
            "compositeImage": {
                "id": "5b3d2d1f-ac5d-4fa2-be78-d9c95ffc9cd6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "49633122-63ee-49ae-b730-7edb78d7c817",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f424033-068a-4f52-91ff-101337c37585",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "49633122-63ee-49ae-b730-7edb78d7c817",
                    "LayerId": "e4f088ce-7398-4e07-8152-ec2436fdb9f3"
                }
            ]
        },
        {
            "id": "36e28998-93ab-47e4-a75c-2d343dd230de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61894e4d-f8a2-48fd-bdcc-7dfd712a2637",
            "compositeImage": {
                "id": "80e3b67b-7bfd-4f0b-b109-1f342636517b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "36e28998-93ab-47e4-a75c-2d343dd230de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "24eafff2-b646-42fe-af63-73ce5deb3333",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "36e28998-93ab-47e4-a75c-2d343dd230de",
                    "LayerId": "e4f088ce-7398-4e07-8152-ec2436fdb9f3"
                }
            ]
        },
        {
            "id": "4834e99b-774a-4755-9e8a-c193a6be2cc6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61894e4d-f8a2-48fd-bdcc-7dfd712a2637",
            "compositeImage": {
                "id": "411b7e37-5fc1-4b69-a97a-355fe133f7aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4834e99b-774a-4755-9e8a-c193a6be2cc6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa34cd94-8204-4d54-96ef-d929b03b6c07",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4834e99b-774a-4755-9e8a-c193a6be2cc6",
                    "LayerId": "e4f088ce-7398-4e07-8152-ec2436fdb9f3"
                }
            ]
        },
        {
            "id": "d4266488-821d-4749-a887-acb1ef81127d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61894e4d-f8a2-48fd-bdcc-7dfd712a2637",
            "compositeImage": {
                "id": "002a52e1-5a6f-4e73-a4e8-6a5bffcda690",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4266488-821d-4749-a887-acb1ef81127d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12f760fd-bcc4-41eb-a877-3dd6209f01c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4266488-821d-4749-a887-acb1ef81127d",
                    "LayerId": "e4f088ce-7398-4e07-8152-ec2436fdb9f3"
                }
            ]
        },
        {
            "id": "88c8bbfe-1bc4-44b6-bba5-6c0b4328299d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61894e4d-f8a2-48fd-bdcc-7dfd712a2637",
            "compositeImage": {
                "id": "9cb25134-f51b-4b10-8b30-f4eac6fe83c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88c8bbfe-1bc4-44b6-bba5-6c0b4328299d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d4cb9075-559f-4c47-bb09-080301e99153",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88c8bbfe-1bc4-44b6-bba5-6c0b4328299d",
                    "LayerId": "e4f088ce-7398-4e07-8152-ec2436fdb9f3"
                }
            ]
        },
        {
            "id": "88bf9818-7027-47b4-9bf8-9024980a62c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61894e4d-f8a2-48fd-bdcc-7dfd712a2637",
            "compositeImage": {
                "id": "4e4710f8-d976-4cc2-9922-ffb6adca3973",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88bf9818-7027-47b4-9bf8-9024980a62c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f464016-f7dc-4b7a-90c8-82c143eab9e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88bf9818-7027-47b4-9bf8-9024980a62c8",
                    "LayerId": "e4f088ce-7398-4e07-8152-ec2436fdb9f3"
                }
            ]
        },
        {
            "id": "e863fa90-35cf-4ce5-ab4b-4fcd0de68c99",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61894e4d-f8a2-48fd-bdcc-7dfd712a2637",
            "compositeImage": {
                "id": "0b75c4b4-24ad-4444-a0dd-eb27a2766683",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e863fa90-35cf-4ce5-ab4b-4fcd0de68c99",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "accdc0ac-9656-4877-a9c9-80f1537cf23c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e863fa90-35cf-4ce5-ab4b-4fcd0de68c99",
                    "LayerId": "e4f088ce-7398-4e07-8152-ec2436fdb9f3"
                }
            ]
        },
        {
            "id": "3aaa5bfc-d77b-43e4-9962-f2d15ffaed6f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61894e4d-f8a2-48fd-bdcc-7dfd712a2637",
            "compositeImage": {
                "id": "4cd3ef15-4503-4015-a33d-2fcc11463327",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3aaa5bfc-d77b-43e4-9962-f2d15ffaed6f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "40eb0170-c036-4e26-ba9e-9fdd4eb6e2f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3aaa5bfc-d77b-43e4-9962-f2d15ffaed6f",
                    "LayerId": "e4f088ce-7398-4e07-8152-ec2436fdb9f3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 97,
    "layers": [
        {
            "id": "e4f088ce-7398-4e07-8152-ec2436fdb9f3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "61894e4d-f8a2-48fd-bdcc-7dfd712a2637",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 6,
    "originLocked": false,
    "playbackSpeed": 30,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 72,
    "xorig": 0,
    "yorig": 96
}