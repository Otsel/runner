{
    "id": "3bbda542-9de7-4a36-a4e4-8f127b63a83e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite0",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e54f852d-b2db-4bd5-bca2-bdb95747953b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3bbda542-9de7-4a36-a4e4-8f127b63a83e",
            "compositeImage": {
                "id": "0c4e0926-5cc4-4e47-bff0-dba457469821",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e54f852d-b2db-4bd5-bca2-bdb95747953b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b043b37-7466-4b9a-a42a-a110ec9715a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e54f852d-b2db-4bd5-bca2-bdb95747953b",
                    "LayerId": "8dd2bc42-0e83-4d24-a7df-8f4cf3f22010"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "8dd2bc42-0e83-4d24-a7df-8f4cf3f22010",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3bbda542-9de7-4a36-a4e4-8f127b63a83e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}